package com.dgss.authentication.api.config;

import javax.servlet.Filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.dgss.authentication.api.filter.ExceptionHandlerFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Bean
	public ExceptionHandlerFilter errorHandlingauthenticationTokenFilterBean() throws Exception {
		ExceptionHandlerFilter authenticationTokenFilter = new ExceptionHandlerFilter();
		return authenticationTokenFilter;
	}

	@Bean
	public FilterRegistrationBean errorHandlerfilterRegistrationBean() throws Exception {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setEnabled(false);
		filterRegistrationBean.setFilter((Filter) errorHandlingauthenticationTokenFilterBean());
		filterRegistrationBean.setOrder(1);
		return filterRegistrationBean;
	}

	protected void configure(HttpSecurity httpSecurity) throws Exception {
		((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl) ((HttpSecurity) ((HttpSecurity) httpSecurity.csrf()
				.disable()).sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and())
						.antMatcher("/**").authorizeRequests().anyRequest()).permitAll();
		httpSecurity.addFilterBefore((Filter) errorHandlingauthenticationTokenFilterBean(),
				UsernamePasswordAuthenticationFilter.class);
	}

//	@Override
//	public void configure(WebSecurity web) throws Exception {
//		web.ignoring().antMatchers("/auth/signin", "/auth/signout", "/register/auth/user", "/v2/api-docs",
//				"/configuration/ui", "/swagger-resources/**", "/configuration/security", "/swagger-ui.html",
//				"/webjars/**");
//	}

}