package com.dgss.authentication.api.config.properties.listener;

import java.io.IOException;
import java.util.Base64;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.stereotype.Component;

import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DatabasePropertiesListener implements ApplicationListener<ApplicationStartedEvent> {
	private static final Logger logger = LoggerFactory.getLogger(DatabasePropertiesListener.class);
	private final static String SPRING_DATASOURCE_USERNAME = "spring.datasource.username";
	private final static String SPRING_DATASOURCE_PASSWORD = "spring.datasource.password";
	private static final String secretName = "dgss/dev";
	private static final String region = "ap-south-1";

	private ObjectMapper mapper = new ObjectMapper();

	@Override
	public void onApplicationEvent(ApplicationStartedEvent event) {
		String secretJson = getSecret();
		String dbUser = getString(secretJson, "username");
		String dbPassword = getString(secretJson, "password");

		ConfigurableEnvironment environment = event.getApplicationContext().getEnvironment();
		Properties props = new Properties();
		props.put(SPRING_DATASOURCE_USERNAME, dbUser);
		props.put(SPRING_DATASOURCE_PASSWORD, dbPassword);
		environment.getPropertySources().addFirst(new PropertiesPropertySource("aws.secret.manager", props));

	}

	private String getSecret() {
		String secret = null;
		String decodedBinarySecret = null;
		GetSecretValueRequest secretValueRequest = new GetSecretValueRequest().withSecretId(secretName);
		AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard().withRegion(region).build();
		GetSecretValueResult secretValueResult = null;

		secretValueResult = client.getSecretValue(secretValueRequest);

		if (secretValueResult.getSecretString() != null) {
			secret = secretValueResult.getSecretString();
		} else {
			decodedBinarySecret = new String(Base64.getDecoder().decode(secretValueResult.getSecretBinary()).array());
		}
		return secret != null ? secret : decodedBinarySecret;
	}

	private String getString(String json, String path) {
		try {
			JsonNode root = mapper.readTree(json);
			return root.path(path).asText();
		} catch (IOException e) {
			logger.error("Can't get {} from json {}", path, json, e);
			return null;
		}
	}

}
