package com.dgss.authentication.api.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dgss.authentication.api.util.AwsUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Configuration
public class DataSourceConfig {
	private static final String USER_NAME = "username";
	private static final String USER_SECRET = "password";
	private static final String HOST = "host";
	private static final String URL = "jdbc:mysql://";
	@Value("${aws.secretsmanager.dbSchema}")
	private String dbSchema;

	@Bean
	public DataSource getDataSource() throws JsonMappingException, JsonProcessingException {
		String secretJson = AwsUtil.getSecret();
		String dbUser = AwsUtil.getString(secretJson, USER_NAME);
		String dbPassword = AwsUtil.getString(secretJson, USER_SECRET);
		String host = AwsUtil.getString(secretJson, HOST);

		DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
		dataSourceBuilder.url(URL +host +dbSchema);
		dataSourceBuilder.username(dbUser);
		dataSourceBuilder.password(dbPassword);
		return dataSourceBuilder.build();
	}
}
