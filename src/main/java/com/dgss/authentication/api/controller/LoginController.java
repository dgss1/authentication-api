package com.dgss.authentication.api.controller;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dgss.authentication.api.dto.AuthenticationResponse;
import com.dgss.authentication.api.dto.JwtAuthenticationRequest;
import com.dgss.authentication.api.dto.TokenRequest;
import com.dgss.authentication.api.exception.IncorrectUserNamePasswordException;
import com.dgss.authentication.api.model.User;
import com.dgss.authentication.api.model.UserSession;
import com.dgss.authentication.api.service.AuthenticationService;
import com.dgss.authentication.api.service.JwtTokenService;
import com.dgss.authentication.api.service.UserSessionService;
import com.dgss.authentication.api.util.AwsUtil;

@RestController
@RequestMapping("/auth")
public class LoginController {
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private JwtTokenService jwtTokenService;
	@Autowired
	private UserSessionService userSessionService;

	@PostMapping(path = "/signin")
	public ResponseEntity<?> login(@RequestBody JwtAuthenticationRequest jwtAuthenticationRequest)
			throws IncorrectUserNamePasswordException, UnsupportedEncodingException, GeneralSecurityException {
		AwsUtil.getSecret();
		User user = authenticationService.autheticateUser(jwtAuthenticationRequest.getuserName());
		if (authenticationService.login(user, jwtAuthenticationRequest.getPassword())) {
			AuthenticationResponse authenticationResponse = new AuthenticationResponse(jwtTokenService.generate(user));
			userSessionService.saveUserSession(authenticationResponse.getJwt(), user);
			return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
		} else {
			throw new IncorrectUserNamePasswordException("Username or Password not found!");
		}
	}

	@PostMapping(path = "/signout")
	public ResponseEntity<?> logout(@RequestBody TokenRequest tokenRequest) {
		UserSession userSession = userSessionService.findByToken(tokenRequest.getToken());
		Map<String, Long> jsonObject = new HashMap<>();
		if (null != userSession) {
			userSessionService.delete(userSession);
			jsonObject.put("userId", userSession.getUserId());
			return new ResponseEntity<String>("Success!", HttpStatus.OK);
		}
		return new ResponseEntity<String>("Failed!", HttpStatus.OK);
	}
}
