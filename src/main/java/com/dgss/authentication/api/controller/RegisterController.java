package com.dgss.authentication.api.controller;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dgss.authentication.api.dto.AuthUserDtoToEntity;
import com.dgss.authentication.api.dto.UserRegisterDetailsRequest;
import com.dgss.authentication.api.model.User;
import com.dgss.authentication.api.service.UserPasswordService;
import com.dgss.authentication.api.service.UserService;

@RestController
@RequestMapping("/register/auth/user")
public class RegisterController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserPasswordService userPasswordService;
  @PostMapping("/register")
  public ResponseEntity<User> registerUser(
      @Valid @RequestBody UserRegisterDetailsRequest userRegisterDetailsRequest) throws UnsupportedEncodingException, GeneralSecurityException {
	  User user = AuthUserDtoToEntity.mapUser(userRegisterDetailsRequest);
		userService.save(user);
		userRegisterDetailsRequest.setUserId(user.getUserId());
		userPasswordService.save(AuthUserDtoToEntity.mapUserPassword(userRegisterDetailsRequest));
		return new ResponseEntity<User>(user, HttpStatus.CREATED);
  }
}
