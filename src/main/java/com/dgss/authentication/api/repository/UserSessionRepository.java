package com.dgss.authentication.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dgss.authentication.api.model.UserSession;

public interface UserSessionRepository extends JpaRepository<UserSession, Integer>{
	public UserSession findByToken(String token);

}
