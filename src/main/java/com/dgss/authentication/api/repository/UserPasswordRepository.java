package com.dgss.authentication.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dgss.authentication.api.model.UserPassword;

public interface UserPasswordRepository extends JpaRepository<UserPassword, Long>{
	public UserPassword findByUserId(Long userId);
}
