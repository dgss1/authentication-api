package com.dgss.authentication.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dgss.authentication.api.model.User;
@Repository
public interface UserReository extends JpaRepository<User, Long>{
	public User findByUserName(String userName);

}
