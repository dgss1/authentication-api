package com.dgss.authentication.api.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.dgss.authentication.api.dto.UserRegisterDetailsRequest;

public class ConfirmPasswordValidator implements ConstraintValidator<ConfirmPassword, Object> {

  @Override
  public boolean isValid(Object user, ConstraintValidatorContext context) {
    String password = ((UserRegisterDetailsRequest)user).getPassword();
    String confirmPassword = ((UserRegisterDetailsRequest)user).getConfirmPassword();
    return password.equals(confirmPassword);
  }

}
