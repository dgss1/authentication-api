package com.dgss.authentication.api.util;

import java.io.IOException;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AwsUtil {
	private static final Logger logger = LoggerFactory.getLogger(AwsUtil.class);
	private static final String secretName = "dgss/dev";
	private static final String region = "ap-south-1";
	private static final ObjectMapper mapper = new ObjectMapper();
	
	public static String getSecret() {
		String secret = null;
		String decodedBinarySecret = null;
		GetSecretValueRequest secretValueRequest = new GetSecretValueRequest().withSecretId(secretName);
		AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard().withRegion(region).build();
		GetSecretValueResult secretValueResult = null;

		secretValueResult = client.getSecretValue(secretValueRequest);

		if (secretValueResult.getSecretString() != null) {
			secret = secretValueResult.getSecretString();
		} else {
			decodedBinarySecret = new String(Base64.getDecoder().decode(secretValueResult.getSecretBinary()).array());
		}
		return secret != null ? secret : decodedBinarySecret;
	}
	
	public static String getString(String json, String path) {
		try {
			JsonNode root = mapper.readTree(json);
			return root.path(path).asText();
		} catch (IOException e) {
			logger.error("Can't get {} from json {}", path, json, e);
			return null;
		}
	}
}
