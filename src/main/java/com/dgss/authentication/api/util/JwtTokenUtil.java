package com.dgss.authentication.api.util;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.dgss.authentication.api.model.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil {
	private static final  long JWT_TOKEN_VALIDITY = 5 * 60 * 60;
	private static final SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS512;
	
	@Value("${security.jwt.key-store}")
	private String SECRET_KEY;

	public String extractUsername(String token) {
		return extractClaim(token, Claims::getSubject);
	}

	public Date extractExpiration(String token) {
		return extractClaim(token, Claims::getExpiration);
	}

	public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = extractAllClaims(token);
		return claimsResolver.apply(claims);
	}

	private Claims extractAllClaims(String token) {
		return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
	}

	private Boolean isTokenExpired(String token) {
		return extractExpiration(token).before(new Date());
	}

	public String generateToken(User user) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("userId", user.getUserId());
		claims.put("userName", user.getUserName());
		claims.put("emaiId", user.getEmailId());
		claims.put("mobileNumber", user.getMobileNumber());

		return createJWT(claims);
	}

	public String createJWT(Map<String, Object> claims) {
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		JwtBuilder builder = Jwts.builder().
				setClaims(claims)
			    //.setId(claims.get("userName").toString()).
			    .setIssuedAt(now).
				setSubject(claims.get("userName").toString()).
				setIssuer(claims.get("userName").toString())
			   .signWith(signatureAlgorithm, signingKey);

		if (JWT_TOKEN_VALIDITY > 0) {
			long expMillis = nowMillis + JWT_TOKEN_VALIDITY;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}

		return builder.compact();
	}

	public Claims decodeJWT(String jwt) {
	    Claims claims = Jwts.parser()
	            .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
	            .parseClaimsJws(jwt).getBody();
	    return claims;
	}
	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = extractUsername(token);
		return (!isTokenExpired(token) && username.equals(userDetails.getUsername()));
	}
}
