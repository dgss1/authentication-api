package com.dgss.authentication.api.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenRequest  implements Serializable{
	private static final long serialVersionUID = -6962904290013828878L;
	private String token;
}
