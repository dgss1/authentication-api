package com.dgss.authentication.api.dto;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;

import com.dgss.authentication.api.model.User;
import com.dgss.authentication.api.model.UserPassword;
import com.dgss.authentication.api.util.EncriptionUtil;

public class AuthUserDtoToEntity {

	public static User mapUser(UserRegisterDetailsRequest userRegisterDetailsRequest) {
		User user = new User();
		user.setUserName(userRegisterDetailsRequest.getUserName());
		user.setActive(Boolean.FALSE);
		user.setCreatedBy("Admin");
		user.setCreatedDate(LocalDateTime.now());
		user.setCreatedByUserId(1);
		return user;
	}

	public static UserPassword mapUserPassword(UserRegisterDetailsRequest userRegisterDetailsRequest)
			throws UnsupportedEncodingException, GeneralSecurityException {
		UserPassword userPassword = new UserPassword();
		userPassword.setUserId(userRegisterDetailsRequest.getUserId());
		userPassword.setSaltedHash(EncriptionUtil
				.hashPassword(userRegisterDetailsRequest.getUserName(), userRegisterDetailsRequest.getPassword())
				.get());
		userPassword.setCreatedBy("Admin");
		userPassword.setDateLastChanged(LocalDateTime.now());
		userPassword.setCreatedDate(LocalDateTime.now());
		userPassword.setCreatedByUserId(1);
		return userPassword;
	}

}
