package com.dgss.authentication.api.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.dgss.authentication.api.validator.ConfirmPassword;
import com.dgss.authentication.api.validator.PasswordValid;
import com.dgss.authentication.api.validator.UniqueUserName;

import lombok.Data;
@Data
@ConfirmPassword
public class UserRegisterDetailsRequest  implements Serializable{
	private static final long serialVersionUID = 1582213346483859554L;
	@Email
	@NotEmpty(message = "please enter email")
	private String email;
	@Size(max = 12)
	@Pattern(regexp = "^[0-9]*$")
	@NotEmpty(message = "please enter mobile no.")
	private String mobileNo;
	@Email
	@UniqueUserName
	@NotEmpty(message = "please enter username")
	private String userName;
	@PasswordValid
	@NotEmpty(message = "please enter your password")
	private String password;
	@NotEmpty(message = "please confirm your password")
	private String confirmPassword;
	private Long userId;
}	
