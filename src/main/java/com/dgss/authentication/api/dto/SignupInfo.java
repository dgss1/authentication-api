package com.dgss.authentication.api.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SignupInfo  implements Serializable{
	private static final long serialVersionUID = -4344561952636171797L;
	private String userName;
	private String password;
	private Boolean Active;
	private String roles;
}
