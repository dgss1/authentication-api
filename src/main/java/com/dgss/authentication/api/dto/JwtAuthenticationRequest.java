package com.dgss.authentication.api.dto;

import java.io.Serializable;

public class JwtAuthenticationRequest implements Serializable {
	private static final long serialVersionUID = -4012082641070260281L;
	private String userName;
	private String password;

	public JwtAuthenticationRequest() {

	}

	public JwtAuthenticationRequest(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}

	public String getuserName() {
		return userName;
	}

	public void setuserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
