package com.dgss.authentication.api.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TokenInfo  implements Serializable{
	private static final long serialVersionUID = -824596968044577689L;
	private Long userId;
	private String userName;
	private String fullName;
	private String roles;
}
