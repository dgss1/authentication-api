package com.dgss.authentication.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
@Table(name = "user")
public class User  implements Serializable{
	private static final long serialVersionUID = 6680713167684559694L;
	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;
	@Column(name = "user_name")
	private String userName;
	@Column(name = "email_id")
	private String emailId;
	@Column(name = "mobile_number")
	private Integer mobileNumber;
	@Column(name = "active")
	private Boolean active;
	
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "created_date")
	private LocalDateTime createdDate; 
	@Column(name = "created_by_user_id")
	private Integer createdByUserId;
	
	@Column(name = "modified_by")
	private String modifiedBy;
	@Column(name = "modified_date")
	private LocalDateTime modifiedDate; 
	@Column(name = "modified_by_user_id")
	private Integer modifiedByUserId;
	@Column(name = "deleted")
	private Boolean deleted = Boolean.FALSE;
	@Column(name = "deleted_by")
	private String deletedBy;
	@Column(name = "deleted_date")
	private LocalDateTime deletedDate;
	@Column(name = "locked")
	private Boolean locked = Boolean.FALSE;
}

	