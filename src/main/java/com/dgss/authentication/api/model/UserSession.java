package com.dgss.authentication.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "auth_user_session")
public class UserSession  implements Serializable{
	private static final long serialVersionUID = -8329707746500338121L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_session_id")
	private Integer userSessionId;
	@Column(name = "user_id")
	private Long userId;
	@Column(name = "token")
	private String token;
	@Column(name = "login_date")
	private LocalDateTime loginDate;
	@Column(name = "last_action_date")
	private LocalDateTime lastActionDate;
	@Column(name = "created_date")
	private LocalDateTime createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "created_by_user_id")
	private Long createdByUserId;
	@Column(name = "modified_date")
	private LocalDateTime modifiedDate;
	@Column(name = "modified_by")
	private String modifiedBy;
	@Column(name = "modified_by_user_id")
	private Long modifiedByUserId;
	@Column(name = "deleted")
	private Boolean deleted;
	@Column(name = "deleted_date")
	private LocalDateTime deletedDate;
	@Column(name = "deleted_by")
	private String deletedBy;
	@Column(name = "deleted_by_user_id")
	private Long deletedByUserId;
	@Column(name = "locked")
	private Boolean locked;
	@Column(name = "version")
	@Version
	private Long version;
}
