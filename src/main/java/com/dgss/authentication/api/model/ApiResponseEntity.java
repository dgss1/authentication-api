package com.dgss.authentication.api.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponseEntity  implements Serializable{
	private static final long serialVersionUID = -8033152674061786373L;
	private  String data ;
	private Boolean status;

}
