package com.dgss.authentication.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "user_password")
public class UserPassword  implements Serializable{
	private static final long serialVersionUID = -3265729696460454859L;
	@Id
	@Column(name = "password_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long passwordId;
	@Column(name = "user_id")
	private Long userId;
	@Column(name = "date_last_changed")
	private LocalDateTime dateLastChanged;
	@Column(name = "salted_hash")
	private String saltedHash;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "created_date")
	private LocalDateTime createdDate;
	@Column(name = "created_by_user_id")
	private Integer createdByUserId;
	@Column(name = "modified_by")
	private String modifiedBy;
	@Column(name = "modified_date")
	private LocalDateTime modifiedDate;
	@Column(name = "modified_by_user_id")
	private Integer modifiedByUserId;
	@Column(name = "deleted")
	private Boolean deleted = Boolean.FALSE;
	@Column(name = "deleted_by")
	private String deletedBy;
	@Column(name = "deleted_date")
	private LocalDateTime deletedDate;
	@Column(name = "locked")
	private Boolean locked = Boolean.FALSE;
}
