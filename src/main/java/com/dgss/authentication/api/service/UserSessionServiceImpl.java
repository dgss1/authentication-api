package com.dgss.authentication.api.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dgss.authentication.api.model.User;
import com.dgss.authentication.api.model.UserSession;
import com.dgss.authentication.api.repository.UserSessionRepository;

@Service
public class UserSessionServiceImpl implements UserSessionService {
	@Autowired
	private UserSessionRepository userSessionRepository;

	@Override
	public void saveUserSession(String token, User user) {
		LocalDateTime now = LocalDateTime.now();
		UserSession userSession = new UserSession();
		userSession.setUserId(user.getUserId());
		userSession.setToken(token);
		userSession.setLoginDate(now);
		userSession.setLastActionDate(now);
		userSession.setCreatedBy(user.getUserName());
		userSession.setCreatedByUserId(user.getUserId());
		userSession.setLocked(Boolean.valueOf(false));
		userSession.setDeleted(Boolean.valueOf(false));
		userSessionRepository.save(userSession);
	}

	@Override
	public UserSession findByToken(String token) {
		// TODO Auto-generated method stub
		return userSessionRepository.findByToken(token);
	}

	@Override
	public void delete(UserSession userSession) {
		userSessionRepository.delete(userSession);
	}
}
