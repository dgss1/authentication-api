package com.dgss.authentication.api.service;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import com.dgss.authentication.api.model.User;

public interface AuthenticationService {
	public User autheticateUser(String userName);
	public Boolean login(User user, String password) throws UnsupportedEncodingException, GeneralSecurityException;
	
}
