package com.dgss.authentication.api.service;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.dgss.authentication.api.model.User;


@Service
public interface UserService {
	public void save(User user) throws UsernameNotFoundException;
	public User findByUserName(String userName);
	
}
