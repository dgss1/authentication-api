package com.dgss.authentication.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.dgss.authentication.api.model.User;
import com.dgss.authentication.api.repository.UserReository;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserReository userRepsitory;

	@Override
	public void save(User user) throws UsernameNotFoundException {
		userRepsitory.save(user);
	}

	@Override
	public User findByUserName(String userName) {
		User user = userRepsitory.findByUserName(userName);
		if (user == null) {
			throw new UsernameNotFoundException(String.format("user not found for user name {}", userName));
		}
		return user;
	}
}
