package com.dgss.authentication.api.service;

import com.dgss.authentication.api.model.User;
import com.dgss.authentication.api.model.UserSession;

public interface UserSessionService {
	public void saveUserSession(String token, User user);
	public UserSession findByToken(String token);
	public void delete(UserSession userSession);
}
