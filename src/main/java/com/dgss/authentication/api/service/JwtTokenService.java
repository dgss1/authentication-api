package com.dgss.authentication.api.service;

import com.dgss.authentication.api.model.User;

public interface JwtTokenService {
	public String generate(User user);
}
