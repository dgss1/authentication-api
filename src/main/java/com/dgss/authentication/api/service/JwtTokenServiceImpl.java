package com.dgss.authentication.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dgss.authentication.api.model.User;
import com.dgss.authentication.api.util.JwtTokenUtil;

@Service
public class JwtTokenServiceImpl implements JwtTokenService{
	@Autowired
	private JwtTokenUtil jwtUtil;
	@Override
	public String generate(User user) {
		return jwtUtil.generateToken(user);
	}
	
}
