package com.dgss.authentication.api.service;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dgss.authentication.api.model.User;
import com.dgss.authentication.api.model.UserPassword;
import com.dgss.authentication.api.util.EncriptionUtil;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
	@Autowired
	private UserService userService;
	@Autowired
	private UserPasswordService userPasswordService;

	@Override
	public User autheticateUser(String userName) {
		return userService.findByUserName(userName);
	}

	@Override
	public Boolean login(User user, String password)
			throws UnsupportedEncodingException, GeneralSecurityException {
		UserPassword userPassword = userPasswordService.findByUserId(user.getUserId());
		String encriptedPassword = EncriptionUtil.hashPassword(user.getUserName(), password).get();
		return encriptedPassword.equals(userPassword.getSaltedHash());
	}
}
