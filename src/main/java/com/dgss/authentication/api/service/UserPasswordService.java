package com.dgss.authentication.api.service;

import com.dgss.authentication.api.model.UserPassword;

public interface UserPasswordService {
	public void save(UserPassword userPassword);
	public UserPassword findByUserId(Long userId);
}
