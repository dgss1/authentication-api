package com.dgss.authentication.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dgss.authentication.api.model.UserPassword;
import com.dgss.authentication.api.repository.UserPasswordRepository;

@Service
public class UserPasswordServiceImpl implements UserPasswordService {
	@Autowired
	private UserPasswordRepository passwordRepository;

	@Override
	public void save(UserPassword userPassword) {
		passwordRepository.save(userPassword);
	}

	@Override
	public UserPassword findByUserId(Long userId) {
		return passwordRepository.findByUserId(userId);
	}
}
