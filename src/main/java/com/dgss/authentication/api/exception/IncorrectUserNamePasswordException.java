package com.dgss.authentication.api.exception;

public class IncorrectUserNamePasswordException extends Exception  {

  private static final long serialVersionUID = 1L;

  public IncorrectUserNamePasswordException(String errorMsg) {
    super(errorMsg);
  }

}
