package com.dgss.authentication.api.exception.handler;

import javax.validation.ConstraintViolationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.dgss.authentication.api.exception.IncorrectUserNamePasswordException;
import com.dgss.authentication.api.exception.ValidationFailedException;
import com.dgss.authentication.api.exception.vo.ErrorResponse;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	private final Logger LOGGER = LogManager.getLogger(getClass());

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<ErrorResponse> handleUnknownSystemException(WebRequest request, Exception e) {
		String message = e.getMessage();
		this.LOGGER.error("handleUnknownSystemException().." + message, e);
		String errorCode = HttpStatus.INTERNAL_SERVER_ERROR.name();
		String errorMessage = "server side error";
		Throwable cause = e.getCause();
		if (cause != null)
			message = message + ", " + cause.getMessage();
		ErrorResponse errorResponse = new ErrorResponse(errorCode, Integer.valueOf(1),
				(message == null) ? errorMessage : message);
		return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<ErrorResponse> handleConstraintViolationException(WebRequest request,
			ConstraintViolationException e) {
		this.LOGGER.error("handleConstraintViolationException().." + e.getMessage(), (Throwable) e);
		String errorCode = HttpStatus.UNPROCESSABLE_ENTITY.name();
		String errorMessage = "server side error";
		ErrorResponse errorResponse = new ErrorResponse(errorCode, Integer.valueOf(1), errorMessage);
		return new ResponseEntity<>(errorResponse, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@ExceptionHandler({ HttpStatusCodeException.class })
	public ResponseEntity<ErrorResponse> handleHttpRequestException(WebRequest request, HttpStatusCodeException e) {
		this.LOGGER.error("handleHttpRequestException().." + e.getMessage(), (Throwable) e);
		String errorCode = HttpStatus.valueOf(e.getRawStatusCode()).name();
		String errorMessage = "server side error";
		ErrorResponse errorResponse = new ErrorResponse(errorCode, Integer.valueOf(1), errorMessage);
		return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(e.getRawStatusCode()));
	}

	@ExceptionHandler({ ValidationFailedException.class })
	public ResponseEntity<ErrorResponse> handleValidationFailedException(WebRequest request,
			ValidationFailedException e) {
		this.LOGGER.error("handleValidationFailedException().." + e.getMessage(), (Throwable) e);
		String errorCode = HttpStatus.BAD_REQUEST.name();
		String errorMessage = e.getMessage();
		ErrorResponse errorResponse = new ErrorResponse(errorCode, Integer.valueOf(1), errorMessage);
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ IncorrectUserNamePasswordException.class })
	public ResponseEntity<ErrorResponse> handleIncorrectUserNamePasswordException(WebRequest request,
			IncorrectUserNamePasswordException e) {
		this.LOGGER.error("handleIncorrectUserNamePasswordException().." + e.getMessage(), (Throwable) e);
		String errorCode = HttpStatus.UNAUTHORIZED.name();
		String errorMessage = "server side error";
		ErrorResponse errorResponse = new ErrorResponse(errorCode, Integer.valueOf(1),
				(e.getMessage() == null) ? errorMessage : e.getMessage());
		return new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);
	}

	public ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		this.LOGGER.error("TypeMismatchException().." + ex.getMessage(), (Throwable) ex);
		String errorCode = HttpStatus.BAD_REQUEST.name();
		String errorMessage = "http message argument type mismatch";
		ErrorResponse errorResponse = new ErrorResponse(errorCode, Integer.valueOf(1), errorMessage);
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		this.LOGGER.error("handleHttpMessageNotReadable().." + ex.getMessage(), (Throwable) ex);
		String errorCode = HttpStatus.BAD_REQUEST.name();
		String errorMessage = "http message not readable";
		ErrorResponse errorResponse = new ErrorResponse(errorCode, Integer.valueOf(1), errorMessage);
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		this.LOGGER.error("handleMethodArgumentNotValid().." + ex.getMessage(), (Throwable) ex);
		String errorCode = HttpStatus.BAD_REQUEST.name();
		String errorMessage = "http message argument not valid";
		ErrorResponse errorResponse = new ErrorResponse(errorCode, Integer.valueOf(1), errorMessage);
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}
}
